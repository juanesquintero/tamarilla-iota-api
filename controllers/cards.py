import os
import logging
import requests
from dotenv import load_dotenv
from flask import request, jsonify, Blueprint
from schemas.cardSchema import validate_card_schema

from logic.dag_transaction import read_card, write_card


load_dotenv()
_hornet_api_path = os.getenv('API_PATH')
_node_address = os.getenv('ADDRESS')

error_logger = logging.getLogger('error_logger')

Card = Blueprint('Card', __name__)


@Card.route('/transactions/hashes')
def get():
    res = requests.post(
        _hornet_api_path,
        headers={'Content-Type': 'application/json',
                 'X-IOTA-API-Version': '1'},
        json={
            'command': 'findTransactions',
            'addresses': [_node_address]
        }
    )
    hashes_res = response(res, 'hashes')

    if isinstance(hashes_res, tuple):
        return jsonify(hashes_res)

    return jsonify({
        'tail_transaction_hashes': hashes_res
    })


@Card.route('/transactions/<tail_transaction_hash>')
def get_one(tail_transaction_hash):
    card = read_card(tail_transaction_hash)
    if card:
        return jsonify(card)
    else:
        return {'err': 'Not Found'}, 404


@Card.route('/transactions', methods=['POST'])
def post_one():
    body = request.get_json()
    # validate schema
    if not(validate_card_schema(body)):
        return {'error': 'invalid body'}, 400

    tail_transaction_hash = write_card(body)
    if tail_transaction_hash:
        return jsonify({
            'msg': 'transaction saved in IOTA Tangle',
            'tail_transaction_hash': str(tail_transaction_hash)
        })
    else:
        return {'err': 'Error saving transaction in IOTA tangle'}, 500


def response(response, field):
    status = str(response.status_code)
    success = status[0] == '2'
    body = None

    # Validar respuesta 2xx y en formato json
    if success:
        try:
            body = response.json()
            # Extraer campo items
            if field in body.keys():
                # Vaidar si el campo items no se encuantra vacio o nulo
                if body[field]:
                    return body[field]
                return {'msg': 'Not Foud'}, 404
            else:
                return body
        except Exception as e:
            error_logger.error('EXCEPTION: '+str(e), exc_info=True)
    return {'err': 'Not Found'}, 404
