from controllers.cards import Card
from flask import Flask, jsonify, request
from flask_cors import CORS
from dotenv import load_dotenv
import os
import re
import werkzeug
import logging

from utils.utils import *

load_dotenv()

# Flask app config
app = Flask(__name__)
app.config['JSON_SORT_KEYS'] = False

CORS(app)

main_path = '/api/v1'


@app.route(main_path)
@app.route(main_path+'/')
def index():
    return jsonify({'api': 'Tamarilla IOTA API'}), 200


'''LOGGING CONFIGURATION'''
LOG_FORMAT = '%(levelname)s %(asctime)s - %(message)s'

# GENERAL (ALL) LOGS
logging.basicConfig(
    filename=os.getcwd()+'/logs/GENERALS.log',
    level=logging.DEBUG,
    format=LOG_FORMAT
)

# ERROR LOGS
error_logger = logging.getLogger('error_logger')
error_logger.setLevel(logging.ERROR)
file_handler = logging.FileHandler(os.getcwd()+'/logs/ERRORS.log')
file_handler.setFormatter(logging.Formatter(LOG_FORMAT))
error_logger.addHandler(file_handler)
'''END LOGGING CONFIGURATION'''

'''ROUTES'''
# Import Controllers

# Register routes
app.register_blueprint(Card, url_prefix=main_path+'')
'''END ROUTES'''


'''ERRORS'''


@app.errorhandler(404)
def page_not_found(e):
    return jsonify({'err': 'Endpoint Not Found'}), 404


@app.errorhandler(405)
def method_not_allow(e):
    return jsonify({'err': 'Method Not Allowed'}), 405


@app.errorhandler(500)
def handle_500(e):
    error_logger.error(e)
    return jsonify({'err': 'System Server Error'}), 500


@app.errorhandler(Exception)
def handle_exception(e):
    error_logger.error('EXCEPTION: '+str(e))
    return {'err': 'Server runtime error ocurred, if is nesesary contact the Admin.'}, 500


'''END ERRORS'''


# Run server
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000, debug=True)
