# Tarjeta Amarilla IOTA API  

## Folder structure:

    ├───.vscode
    ├───controllers
    ├───db
    ├───logic
    ├───schemas
    ├───utils
    ├───logs
    ├───venv
    ├───.env
    ├───.flaskenv
    ├───.gitignore
    ├───requirements.txt

## Define your environment:
    this project need a python 3.6 version to run properly because PyOTA is incompatoibility with older versions until now 
    
    1. Download the version on your pc 
    https://www.python.org/downloads/release/python-360/
    https://www.python.org/downloads/release/python-368/

    2. Install the virtualenv library 
    $ pip install virtualenv

    3.1 Create the virtualenv with the python version 
    $ virtualenv --python=python3.6 venv
    or
    $ virtualenv --python=/path/to/python/executable venv
    

    3.2 Select the Python3.6 interpreter and open new terminal (VSCode)
    $ python --version

    4. Activate virtualenv
    $ source venv/Scripts/activate
    or 
     $ source venv/bin/activate

## Install it:

    (venv) $ pip install -r requirements-dev.txt
    (venv) $ pip freeze

## Define environment variables:

    (venv) $ touch .env
        API_PATH = ""
        SECRET_KEY = "secret"
        ADDRESS = "SEED9GOES9HERE"

## Run it:

    (venv) $ flask run --reload --with-threads

## Parepare your Editor:

Git Extensions:
<br>
https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens

https://marketplace.visualstudio.com/items?itemName=donjayamanne.githistory

https://marketplace.visualstudio.com/items?itemName=mhutchie.git-graph

<br>
Python Extensions:
<br>

https://marketplace.visualstudio.com/items?itemName=ms-python.python

https://marketplace.visualstudio.com/items?itemName=LittleFoxTeam.vscode-python-test-adapter

https://marketplace.visualstudio.com/items?itemName=njpwerner.autodocstring

https://marketplace.visualstudio.com/items?itemName=mgesbert.python-path

<br>
Flask Extensions:
<br>

https://marketplace.visualstudio.com/items?itemName=cstrap.flask-snippets

https://marketplace.visualstudio.com/items?itemName=wholroyd.jinja

https://marketplace.visualstudio.com/items?itemName=samuelcolvin.jinjahtml

<br>
Linters Extensions:
<br>

https://marketplace.visualstudio.com/items?itemName=usernamehw.errorlens

https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker-spanish

<br>
HTML Templates Extensions:
<br>

https://marketplace.visualstudio.com/items?itemName=formulahendry.auto-close-tag

https://marketplace.visualstudio.com/items?itemName=formulahendry.auto-rename-tag

https://marketplace.visualstudio.com/items?itemName=vincaslt.highlight-matching-tag

<br>
Extras:
<br>

https://marketplace.visualstudio.com/items?itemName=vscode-icons-team.vscode-icons

https://marketplace.visualstudio.com/items?itemName=TabNine.tabnine-vscode

https://marketplace.visualstudio.com/items?itemName=Gruntfuggly.todo-tree

https://marketplace.visualstudio.com/items?itemName=mikestead.dotenv

https://marketplace.visualstudio.com/items?itemName=CoenraadS.bracket-pair-colorizer


<br>

## DocString template 

For Python Docstring Generator VSCode extension

Function

    '''
    [Summary].

    :param   [name]<type>:  [description].

    :returns [name]<type>:  [description].
    '''

Class

    '''
    [Summary].
    '''

## That's it!!

