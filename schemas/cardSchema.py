from jsonschema import Draft7Validator, draft7_format_checker

card_schema = {
    "type": "object",
    "properties": {
        "radicado":  {"type": "string", "maxLength": 50},
        
        "placa_vehiculo":  {"type": "string", "maxLength": 50},
        "tarjeta_vehiculo":  {"type": "string", "maxLength": 50},

        "numero_tarjeta_operacion":  {"type": "string", "maxLength": 100},
        "fecha_vencimiento": {"type": "string", "format": "date-time"},
        "fecha_inicio_vigencia": {"type": "string", "format": "date-time"},
        "fecha_fin_vigencia": {"type": "string", "format": "date-time"},

        "tipo_documento_conductor":  {"type": "string", "maxLength": 50},
        "documento_conductor":  {"type": "string", "maxLength": 50},
        "nombre_conductor":  {"type": "string", "maxLength": 200},
        "licencia_conductor":  {"type": "string", "maxLength": 200},
    },
    "required": [
        "radicado",

        "placa_vehiculo",
        "tarjeta_vehiculo",

        "numero_tarjeta_operacion",
        "fecha_vencimiento",
        "fecha_inicio_vigencia",
        "fecha_fin_vigencia",

        "tipo_documento_conductor",
        "documento_conductor",
        "nombre_conductor",
        "licencia_conductor"
    ],
    "additionalProperties": False
}


def validate_card_schema(json):
    return Draft7Validator(card_schema,format_checker=draft7_format_checker).is_valid(json)
