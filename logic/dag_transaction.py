import os
import json
import logging
from flask import flash
from dotenv import load_dotenv
from iota import Iota, ProposedTransaction, Address, Tag, TryteString


error_logger = logging.getLogger('error_logger')

load_dotenv()
_hornet_api_path = os.getenv('API_PATH')
_node_address = os.getenv('ADDRESS')

HORNET_API = Iota(_hornet_api_path, testnet=True)

def read_card(tail_transaction_hash):
    try:
        # Get the transaction objects in the bundle
        bundle = HORNET_API.get_bundles(tail_transaction_hash)
        # Print the message that's in the first transaction's `signatureMesageFragment` field
        message = bundle['bundles'][0].tail_transaction.signature_message_fragment
        # Convert the message from trytes to ASCII characters
        text = message.decode()
        text_object = json.loads(text)
        return text_object
    except Exception as e:
        error_logger.error('EXCEPTION: '+str(e), exc_info=True)
        return False


def write_card(msg):
    try:
        transaction_object = json.dumps(dict(msg))
        message = TryteString.from_unicode(transaction_object)
        tx = ProposedTransaction(
            address=Address(_node_address),
            message=message,
            value=0
        )
        result = HORNET_API.send_transfer(transfers=[tx])
        tail_transaction_hash = result['bundle'].tail_transaction.hash
        return tail_transaction_hash
    except Exception as e:
        error_logger.error('EXCEPTION: '+str(e), exc_info=True)
        return False
